using OpenQA.Selenium;

namespace GoogleTester
{
    public static class GoogleSearchElements
    {
        public static By InputBox = By.CssSelector("input[name='q']");
        public static By SearchButton = By.CssSelector("input[name='btnK']");
        public static By ImFeelingLuckyButton = By.CssSelector("input[name='btnI']");
    }
}