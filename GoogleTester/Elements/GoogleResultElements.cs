using OpenQA.Selenium;

namespace GoogleTester
{
    public static class GoogleResultElements
    {
        public static By Results = By.XPath("//div[@class='g']");
        public static By TitleLink = By.XPath(".//h3//a");
    }
}