using OpenQA.Selenium;

namespace GoogleTester
{
    public static class GooglePreferencesElements
    {
        public static By InstantPredictionsNever = By.XPath("//div[@id='instant-radio']/div[@data-value='2']/span[@class='jfk-radiobutton-radio']");
        public static By SaveButton = By.XPath("//*[@class='goog-inline-block jfk-button jfk-button-action']");
    }
}