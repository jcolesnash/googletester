﻿using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace GoogleTester
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver _driver;

        [TestFixtureSetUp]
        public void Setup()
        {
            _driver = new FirefoxDriver();
            _driver.EnableImplicitWait();
        }

        [Test]
        public void GoogleSearch_SeleniumIDE_ReturnsCorrectResult()
        {
            var searchPage = new GoogleSearchView(_driver)
                .NavigateTo()
                .SetInputBox("Selenium IDE")
                .ClickSearch();

            var searchResults = searchPage.GetResults();

            var downloadResult = searchResults.First(e => e.Title == "Download Selenium IDE");

            Assert.That(downloadResult != null);
            Assert.That(downloadResult.Url == "http://www.seleniumhq.org/download/");
        }

        [Test]
        public void GoogleSearch_RandomString_ReturnsResults()
        {
            var searchPage = new GoogleSearchView(_driver)
                .NavigateTo()
                .SetInputBox(Helpers.RandomString(3))
                .ClickSearch();

            var searchResults = searchPage.GetResults();

            Assert.That(searchResults.Count > 0);
        }

        [Test]
        public void GoogleImFeelingLucky_Bourbon_ReturnsPage()
        {
            Helpers.DisableGoogleInstantPredictions(_driver);

            new GoogleSearchView(_driver)
                .NavigateTo()
                .SetInputBox("bourbon")
                .ClickImFeelingLucky();

            Assert.That(_driver.Url == "https://en.wikipedia.org/wiki/Bourbon_whiskey");
            Assert.That(_driver.FindElement(By.XPath("//h1[@id='firstHeading']")).Text == "Bourbon whiskey");
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            _driver.Close();
            _driver.Quit();
        }
    }
}
