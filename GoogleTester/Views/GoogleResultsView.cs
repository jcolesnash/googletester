﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace GoogleTester
{
    public class GoogleResultsView
    {
        private IWebDriver _driver;

        public GoogleResultsView(IWebDriver driver)
        {
            _driver = driver;
        }

        public List<GoogleResult> GetResults()
        {
            var list = new List<GoogleResult>();

            var results = _driver.FindElements(GoogleResultElements.Results);

            foreach (var webElement in results)
            {
                list.Add(new GoogleResult
                {
                    Title = webElement.FindElement(GoogleResultElements.TitleLink).Text,
                    Url = webElement.FindElement(GoogleResultElements.TitleLink).GetAttribute("href")
                });
            }
            
            return list;
        }
    }
}
