﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace GoogleTester
{
    public class GoogleSearchView
    {
        private const string Url = "http://google.ca";
        private readonly IWebDriver _driver;

        public GoogleSearchView(IWebDriver driver)
        {
            _driver = driver;
        }

        public GoogleSearchView NavigateTo()
        {
            _driver.Navigate().GoToUrl(Url);

            return this;
        }

        public GoogleSearchView VerifyView()
        {
            _driver.WaitForElement(GoogleSearchElements.InputBox, 10);
            Assert.That(_driver.FindElement(GoogleSearchElements.InputBox).Displayed);
            return this;
        }

        public GoogleSearchView SetInputBox(string value)
        {
            _driver.FindElement(GoogleSearchElements.InputBox).SendKeys(value);
            _driver.FindElement(GoogleSearchElements.InputBox).SendKeys(Keys.Escape);

            return this;
        }

        public string GetInputBox()
        {
            return _driver.FindElement(GoogleSearchElements.InputBox).Text;
        }

        public GoogleResultsView ClickSearch()
        {
            if (_driver.FindElement(GoogleSearchElements.SearchButton).Displayed)
                _driver.FindElement(GoogleSearchElements.SearchButton).Click();

            return new GoogleResultsView(_driver);
        }

        public void ClickImFeelingLucky()
        {
            _driver.FindElement(GoogleSearchElements.ImFeelingLuckyButton).Click();
        }
    }
}
