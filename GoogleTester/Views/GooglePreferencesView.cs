﻿using OpenQA.Selenium;

namespace GoogleTester
{
    public class GooglePreferencesView
    {
        private readonly IWebDriver _driver;
        private const string Url = "https://www.google.ca/preferences";

        public GooglePreferencesView(IWebDriver driver)
        {
            _driver = driver;
        }

        public GooglePreferencesView NavigateTo()
        {
            _driver.Navigate().GoToUrl(Url);

            return this;
        }

        public GooglePreferencesView InstantPredictionsClickNever()
        {
            _driver.FindElement(GooglePreferencesElements.InstantPredictionsNever).Click();

            return this;
        }

        public void ClickSave()
        {
            _driver.FindElement(GooglePreferencesElements.SaveButton).Click();
        }
    }
}
