using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;

namespace GoogleTester
{
    public static class Helpers
    {
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void WaitForElement(this IWebDriver driver, By by, int timeout, int checkInterval = 1000)
        {
            var startTime = DateTime.UtcNow;
            driver.DisableImplicitWait();
            while (DateTime.UtcNow - startTime < TimeSpan.FromSeconds(timeout))
            {
                if (driver.FindElements(by).Count > 0)
                {
                    driver.EnableImplicitWait();
                    return;
                }

                Thread.Sleep(checkInterval);
            }

            driver.EnableImplicitWait();
            throw new Exception(string.Format("Took longer than {0}s to find element", timeout));
        }

        public static void DisableImplicitWait(this IWebDriver driver)
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.Zero);
        }

        public static void EnableImplicitWait(this IWebDriver driver)
        {
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
        }

        public static void DisableGoogleInstantPredictions(IWebDriver driver)
        {
            new GooglePreferencesView(driver)
                .NavigateTo()
                .InstantPredictionsClickNever()
                .ClickSave();
        }
    }
}