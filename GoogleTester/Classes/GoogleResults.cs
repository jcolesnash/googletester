﻿namespace GoogleTester
{
    public class GoogleResult
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
    }
}
