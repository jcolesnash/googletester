# Setup

* Install Visual Studio 2013 community edition (https://www.visualstudio.com/en-us/news/vs2013-community-vs.aspx)
* Install Resharper for VS2013 (https://www.jetbrains.com/resharper/download/)
* Install the latest version of firefox (https://www.mozilla.org/en-US/firefox/new/)
* Download this codebase to your machine
* Open the googletest.sln solution
* Using resharper, run all unit tests